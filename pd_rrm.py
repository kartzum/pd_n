from torch.utils.data import Dataset, DataLoader

from .utils import *
from .data import *
from .models import *


#

class TokenProcessor:
    def process(self, text):
        pass


#

class ModelRunner:
    def run(self, data_loader):
        pass


class PdModelRunner(ModelRunner):
    def __init__(self, model_path):
        self._config = PdModelRunner._Config(model_path)
        model_ = self.prepare_model(self._config)
        self.model = model_.eval()

    def run(self, data_loader):

        sents = []
        pred_set_list = []
        entities = []
        for i, inputs in enumerate(data_loader):
            rets = self.model.predict_step(inputs)
            pred_set_list += list(rets['pred_set'])
            entities += inputs['original_entities']
            sents += inputs['tokens']

        return self.model, sents, pred_set_list, entities

    def prepare_model(self, config):
        ModelClass = eval(config.model_class)
        model = ModelClass(config)
        # print(model)

        # load weight
        if not config.model_read_ckpt:
            raise Exception('Specify model checkpoint')
        # print(f"reading params from {config.model_read_ckpt}")
        model = model.load(config.model_read_ckpt)
        model.token_embedding.token_indexing.update_vocab = False

        return model

    class _Config:
        def __init__(self, model_path=None):
            self.model_class = "PyramidNestNER"
            self.tag_form = "iob2"
            self.maxlen = None
            self.vocab_file = None
            self.cased = 1
            self.char_emb_dim = 30
            self.char_encoder = "lstm"
            self.dropout = 0.4
            self.lm_emb_dim = 0
            self.token_emb_dim = 300
            self.vocab_size = 2000000
            self.device = "cpu"
            self.hidden_dim = 200
            self.max_depth = 16
            self.tag_vocab_size = 30
            self.optimizer = "sgd"
            self.lr = 0.01
            self.model_read_ckpt = model_path

        def copy(self):
            c = PdModelRunner._Config()
            return c


def create_model_runner(model_path):
    return PdModelRunner(model_path)


#

class SimpleLightJsonDataset(Dataset):

    def __init__(self, data, token_processor, tag_form='iob2', skip_empty=False):
        tokens = token_processor.process(data)
        entities = []
        item = {"tokens": tokens, "entities": entities}
        self.json_list = []
        self.json_list.append(item)

    def __len__(self):
        return len(self.json_list)

    def __getitem__(self, idx):
        return self.json_list[idx]


class PyramidNestNERLightDataLoader(DataLoader):

    def __init__(self, dataset_provider,
                 model=None, num_workers=0, tag_form='iob2',
                 skip_empty=False, max_depth=None, *args, **kargs):
        self.model = model
        self.num_workers = num_workers
        self.max_depth = max_depth
        self.dataset = dataset_provider
        super().__init__(dataset=self.dataset, collate_fn=self._collect_fn, num_workers=num_workers, *args, **kargs)

        if self.num_workers == 0:
            pass  # does not need warm indexing
        elif self.model is not None:
            print("warm indexing...")
            tmp = self.num_workers
            self.num_workers = 0
            for batch in self:
                pass
            self.num_workers = tmp
        else:
            print("warn: model is not set, skip warming.")
            print("note that if num_worker>0, vocab will be reset after each batch step,")
            print("thus a warming of indexing is required!")

    def add_entities_to_tags(self, tags, entities, depth):
        span2entities = defaultdict(set)
        for entity in entities:
            start, end = entity['span'][0], entity['span'][1] - depth
            etype = entity['entity_type']
            span2entities[(start, end)].add(etype)

        for (start, end), etypes in span2entities.items():
            etype = '|'.join(sorted(list(etypes)))
            for i in range(start + 1, end):
                tags[i] = f'I-{etype}'
            tags[start] = f'B-{etype}'
        return tags

    def _normalize_nested_labels(self, entities, length, max_depth):
        tmp = defaultdict(list)
        for entity in entities:
            if entity['span'][1] <= length:
                tmp[entity['span'][1] - entity['span'][0] - 1].append(entity)
            else:
                print(f'entity exceeds the given length: {entity}')

        ret = [['O'] * (length - depth) for depth in range(max_depth + 1)]
        for depth in range(max([max_depth, *tmp.keys()]) + 1):
            ents = tmp.get(depth, [])
            depth = min(depth, max_depth)
            ret[depth] = self.add_entities_to_tags(ret[depth], ents, depth)

        return ret

    def _collect_fn(self, batch):
        tokens, labels, entities = [], [], []
        max_depth = self.max_depth if self.max_depth is not None else max(len(item['tokens']) for item in batch) - 1
        for item in batch:
            _tokens = item['tokens'] if self.model is None else item['tokens']  # [:150] # TODO: temporally limit maxlen
            tokens.append(_tokens)
            labels.append(self._normalize_nested_labels(
                item['entities'], length=len(_tokens), max_depth=max_depth))
            entities.append(item['entities'])

        rets = {
            'tokens': tokens,
            'labels': labels,
            'original_entities': entities,
        }

        if self.model is not None:
            tokens = self.model.token_indexing(tokens)  # (B, T)
            labels = self.model.pyramid_tag_indexing(labels)

            rets['_tokens'] = tokens
            rets['_labels'] = labels

        return rets


def create_data_loader(text, token_processor, tag_form='iob2', skip_empty=False, model=None):
    d = SimpleLightJsonDataset(text, token_processor, tag_form, skip_empty)
    return PyramidNestNERLightDataLoader(d, model=model)

#
